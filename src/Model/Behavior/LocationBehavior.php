<?php

/**
 * LocationManagerBehavior
 *
 * PHP version 7
 * 
 * @category Behaviour
 * @package  24WEB.LocationManager
 * @version  V1
 * @author   Paulo Homem <paulo.homem@24web.pt>
 * @license  
 * @link     http://24web.pt
 */

namespace LocationManager\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\Event\Event;
use ArrayObject;
use Cake\ORM\RulesChecker;

class LocationBehavior extends Behavior {

    public function initialize(array $config) {
        $this->setTableSettings();
    }

    private function setTableSettings() {
        $this->getTable()->belongsTo('Locations', [
            'foreignKey' => 'country_id',
            'className' => 'LocationManager.Locations'
        ]);
    }
    
    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(Event $event, RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['country_id'], 'Locations'));
        return $rules;
    }

}
